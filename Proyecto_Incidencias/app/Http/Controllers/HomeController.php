<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\profesores;
use App\Incidencia;
class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    //home normal
    public function index()
    {
        /*
        Si el usuario es admin le mostrara la vista de homeAdmin 
        Si no es admin muestra la vista home 
        En el navegador siempre sera http://localhost:8000/home
        Tanto para el admin como para los usuarios
        */
        $incidencias = Incidencia::select('id','equipo','profesor_ID','aula','codigo','fecha','estado','obs_usu')->orderBy('estado','desc')->orderBy('fecha', 'desc')->paginate(10);
        if(Auth::user()->can('es_admin',$incidencias[0])){
            $profesores = profesores::select('id','name')->get();
            return view('homeAdmin',['todas_las_incidencias'=>$incidencias,'listado_profesores'=>$profesores]);
        }else{
            $incidencias = Incidencia::select('id','equipo','profesor_ID','aula','codigo','fecha','estado','obs_usu')->where('profesor_ID', Auth::user()->id)->orderBy('estado','desc')->orderBy('fecha', 'desc')->paginate(10);
            return view('home',['todas_las_incidencias'=>$incidencias]);
        }
    }

    //home solo para las pendientes
    public function pendientes()
    {
        $incidencias = Incidencia::select('id','equipo','profesor_ID','aula','codigo','fecha','estado','obs_usu')->where('estado', 'pendiente')->orderBy('estado','desc')->orderBy('fecha', 'desc')->paginate(10);
        if(Auth::user()->can('es_admin',$incidencias[0])){
            $profesores = profesores::select('id','name')->get();
            return view('homeAdmin',['todas_las_incidencias'=>$incidencias,'listado_profesores'=>$profesores]);
        }else{
            $incidencias = Incidencia::select('id','equipo','profesor_ID','aula','codigo','fecha','estado','obs_usu')->where('estado', 'pendiente')->where('profesor_ID', Auth::user()->id)->orderBy('estado','desc')->orderBy('fecha', 'desc')->paginate(10);
            return view('home',['todas_las_incidencias'=>$incidencias]);
        }
    }

    //home solo para las finalizadas
    public function finalizadas()
    {
        $incidencias = Incidencia::select('id','equipo','profesor_ID','aula','codigo','fecha','estado','obs_usu')->where('estado', 'finalizado')->orderBy('estado','desc')->orderBy('fecha', 'desc')->paginate(10);
        if(Auth::user()->can('es_admin',$incidencias[0])){   
            $profesores = profesores::select('id','name')->get();
            return view('homeAdmin',['todas_las_incidencias'=>$incidencias,'listado_profesores'=>$profesores]);
        }else{
            $incidencias = Incidencia::select('id','equipo','profesor_ID','aula','codigo','fecha','estado','obs_usu')->where('estado', 'finalizado')->where('profesor_ID', Auth::user()->id)->orderBy('estado','desc')->orderBy('fecha', 'desc')->paginate(10);
            return view('home',['todas_las_incidencias'=>$incidencias]);
        }
    }

    //home solo para las canceladas
    public function canceladas()
    {
        $incidencias = Incidencia::select('id','equipo','profesor_ID','aula','codigo','fecha','estado','obs_usu')->where('estado', 'cancelado')->orderBy('estado','desc')->orderBy('fecha', 'desc')->paginate(10);
        if(Auth::user()->can('es_admin',$incidencias[0])){
            $profesores = profesores::select('id','name')->get();
            return view('homeAdmin',['todas_las_incidencias'=>$incidencias,'listado_profesores'=>$profesores]);
        }else{
            $incidencias = Incidencia::select('id','equipo','profesor_ID','aula','codigo','fecha','estado','obs_usu')->where('estado', 'cancelado')->where('profesor_ID', Auth::user()->id)->orderBy('estado','desc')->orderBy('fecha', 'desc')->paginate(10);
            return view('home',['todas_las_incidencias'=>$incidencias]);
        }
    }

    //Funcion para enseñar al usuario sus propias incidencias
    public function ver_incidencia(Request $incidencia)
    {
        $incidencias = Incidencia::select('id','aula','codigo','codigo_otros','equipo','profesor_ID','estado','fecha','obs_usu','obs_adm','admin')->where('id', $incidencia->id)->orderBy('fecha', 'desc')->get();
        $profesor = profesores::select('id','name')->where('id',$incidencias[0]->profesor_ID)->get();
        if($incidencias[0]->admin=="a esperas del admin"){
            $admin = profesores::select('id','name')->where('id',$incidencias[0]->admin)->get();
        };
        if(Auth::user()->can('es_admin',$incidencias[0])){
            return view('ver_incidencia',['todas_las_incidencias'=>$incidencias,'profesor'=>$profesor[0]]);
        }
        
        if(Auth::user()->can('actuar_sobre_la_incidencia',$incidencias[0]))
        {
            return view('ver_incidencia',['todas_las_incidencias'=>$incidencias,'profesor'=>$profesor[0]]);
        }else{
            return view("error",['error'=>"NO PUEDES VER UNA INCIDENCIA QUE NO ES TUYA"]);
        }
    }

    public function modificar_incidencia(Request $incidencia)
    {
        //Funcion para enseñar al usuario sus propias incidencias
        $incidencias = Incidencia::select('id','equipo','profesor_ID','aula','codigo','codigo_otros','fecha','estado','obs_usu')->where('id', $incidencia->id)->orderBy('fecha', 'desc')->get();
            
        if(Auth::user()->can('es_admin',$incidencias[0])){
            return view('mod_incid_admin',['todas_las_incidencias'=>$incidencias]);
        }else{
            
            $incid = Incidencia::find($incidencia->id);

            if(Auth::user()->can('actuar_sobre_la_incidencia',$incid)){
                if($incidencias[0]->estado!="pendiente"){
                    return view("error",['error'=>"ERROR NO SE PUEDE MODIFICAR UNA INCIDENCIA QUE ESTA CERRADA"]);
                }
                return view('mod_incidencias',['todas_las_incidencias'=>$incidencias]);
            }else{
                return view("error",['error'=>"NO PUEDES MODIFICAR UNA INCIDENCIA QUE NO ES TUYA"]);
            }
        }
    }

    public function borrar_incidencia(Request $request)
    {
        $incidencia= Incidencia::select('id','equipo','profesor_ID','aula','codigo','fecha','estado','obs_usu')->where('id', $request->id)->first();
        
        if(Auth::user()->can('actuar_sobre_la_incidencia',$incidencia))
        {
            if($incidencia->estado!="pendiente"){
                return view("error",['error'=>"ERROR NO SE PUEDE BORRAR UNA INCIDENCIA QUE ESTA CERRADA"]);
            }
            $incidencia->delete();
        }else{
            return view("error",['error'=>"NO PUEDES BORRAR UNA INCIDENCIA QUE NO ES TUYA"]);
        }
        return redirect("/ver");
    }
    
}