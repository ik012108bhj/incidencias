<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect,Response,DB,Config;
use Mail;
use App\Incidencia;

class EmailController extends Controller{
    //Funcion para mandar el Email
    public function sendEmail(){
        
        //Declaramiento de variables
        $equipo = session()->get('equipo');
        $email = session()->get('email');
        $estado = session()->get('estado');
        $obs = session()->get('obs');
        $mensaje="";

        //Si viene vacio 
        if($equipo==Null||$email==Null||$estado==Null||$obs==Null){
            return view("error",['error'=>"No se puede mandar el email se ha encontrado un error"]);
        }

        //Creacion de mensaje
        if($estado=="igual"){
            $parte1= 'Se ha revisado su incidencia con el equipo ';
            $parte2= '. Mensaje automatizado, por favor no responder.';
            $mensaje=$parte1.$equipo.$parte2;
        }else{
            $parte1='Se ha revisado su incidencia con el equipo ';
            $parte2=' y su estado ahora es de ';
            $parte3='. Mensaje automatizado, por favor no responder.';
            $mensaje=$parte1.$equipo.$parte2.$estado.$parte3;
        }

        $incidencias = Incidencia::select('id','equipo','profesor_ID','aula','codigo','fecha','estado','obs_usu')->orderBy('estado','desc')->orderBy('fecha', 'desc')->paginate(10);
        
        //Comprobamos si el administrador ha modificado algo o simplemente le ha dado a aceptar sin modificar nada para saber si mandar o no el correo
        if($estado=="igual" && $obs=="igual"){
            return redirect("home");
        }else{
            //Funcion para mandar el correo
            Mail::send([],[], function($message) use ($email,$mensaje) {
                //Correo del destinatario
                $message->to($email, 'Su incidencia ha sido revisada');
                //De quien es
                $message->from('Administrador@gmail.com', 'Administrador');
                //El asunto
                $message->subject('Su incidencia ha sido revisada');
                //El mensaje
                $message->setBody($mensaje);
            });
            
            //Si falla
            if (Mail::failures()) {
                return view("error",['error'=>"No se ha podido mandar el email, se ha encontrado un error"]);
            }else{
                return redirect("home");
            }
        }   
    }
}