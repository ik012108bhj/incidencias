<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Socialite;
use App\Http\Controllers\GoogleController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Mail;
//use Auth;

use Exception;

use App\profesores;
use App\Incidencia;

class GoogleController extends Controller
{

    public function vista_crear_incidencia(Request $request){
        if(!Auth::user()){
            return view("error",['error'=>"NO PUEDES ACCEDER A ESTA RUTA SIN ESTAR LOGUEADO"]);
        }
        if(Auth::user()->admin){
            return view("error",['error'=>"LOS ADMINS NO PUEDEN CREAR INCIDENCIAS"]);
        }
        return view('registro_incidencia');
    }
  
/*
FUNCION PARA CREAR UNA INCIDENCIA
*/
    public function crear_incidencia(Request $request) {
        //pasamos todo a la sesion para poder obtener los datos mediante old, esto es otro metodo parecido al que hicimos en las actividades.
        $request->flash();
        if(!Auth::user()){
            return view("error",['error'=>"NO PUEDES ACCEDER A ESTA RUTA SIN ESTAR LOGUEADO"]);
        }

        if(Auth::user()->admin){
            return view("error",['error'=>"LOS ADMINS NO PUEDEN CREAR INCIDENCIAS"]);
        }
        if(Auth::user()->id!=$request->profesor_ID){
            return view("error",['error'=>"NO PUEDES CREAR UNA INCIDENCIA A NOMBRE DE OTRO"]);
        }
        if($request->codigo=="10.Otros/ Beste batzu"){
            $validator = Validator::make($request->all(), [
                'equipo' => [
                    'required','min:3','max:10','regex:/^hz[0-9]*$/',
                ],
                'aula' =>[
                    'required','min:3','max:5','regex:/^[0-9]*$/',
                ],
                'codigo' =>[
                    'in:10.Otros/ Beste batzu',
                ],
                'obs_usu' =>[
                    'max:255',
                ],
                'otros' =>[
                    'required','max:255',
                ],
                'profesor_ID' =>[
                    'required',
                ],
                'fecha' => [
                    'required','min:10','max:10','regex:/^[0-9]{2}-[0-9]{2}-[0-9]{4}$/',
                ],
            ],
            [
                //Mensajes de error en un campo en especifico
                'equipo.regex' => 'El campo equipo no se ha escrito correctamente es necesario un hz seguido de uno a ocho numeros',
                'fecha.regex' => 'La fecha no tiene un formato valido',
                'aula.regex' => 'En el campo aula solo se admiten numeros',
                'codigo.in' =>'No has seleccionado un error valido',
                'obs_usu.max' => 'El campo observaciones del usuario no puede tener mas de :max caracteres',
                'otros.max' => 'El campo otro error no puede tener mas de :max caracteres',
                //Mensajes de error generales
                'required' => 'El campo :attribute es obligatorio',
                'between' => 'El :attribute valor :input no esta entre :min - :max.',
                'max' => 'El :attribute no puede superar los :max caracteres',
                'min' => 'El :attribute no puede tener menos de :min caracteres',
                
            ]);
    
            if ($validator->fails()) {
                return view('registro_incidencia')
                    ->withErrors($validator)
                    ->withInput($request->all());             
            }
    
        }else{
            //Si la incidencia tiene un error que no sea otro
            $validator = Validator::make($request->all(), [
                'equipo' => [
                    'required','min:3','max:10','regex:/^hz[0-9]*$/',
                ],
                'aula' =>[
                    'required','min:3','max:5','regex:/^[0-9]*$/',
                ],
                'codigo' =>[
                    'in:1.No se enciende la CPU/ CPU ez da pizten,2.No se enciende la pantalla/Pantaila ez da pizten,3.No entra en mi sesión/ ezin sartu nere erabiltzailearekin,4.No navega en Internet/ Internet ez dabil,5.No se oye el sonido/ Ez da aditzen,6.No lee el DVD/CD,7.Teclado roto/ Tekladu hondatuta,8.No funciona el ratón/Xagua ez dabil,9.Muy lento para entrar en la sesión/oso motel dijoa',
                ],
                'obs_usu' =>[
                    'max:255',
                ],
                'otros' =>[
                    'max:0',
                ],
                'profesor_ID' =>[
                    'required',
                ],
                'fecha' => [
                    'required','min:10','max:10','regex:/^[0-9]{2}-[0-9]{2}-[0-9]{4}$/',
                ],
            ],
            [
                //Mensajes de error en un campo en especifico
                'equipo.regex' => 'El campo equipo no se ha escrito correctamente es necesario un hz seguido de uno a ocho numeros',
                'fecha.regex' => 'La fecha no tiene un formato valido',
                'aula.regex' => 'En el campo aula solo se admiten numeros',
                'codigo.in' =>'No has seleccionado un error valido',
                'obs_usu.max' => 'El campo observaciones del usuario no puede tener mas de :max caracteres',
                'otros.max' => 'El campo otros tiene que estar vacio',
                //Mensajes de error generales
                'required' => 'El campo :attribute es obligatorio',
                'between' => 'El :attribute valor :input no esta entre :min - :max.',
                'max' => 'El :attribute no puede superar los :max caracteres',
                'min' => 'El :attribute no puede tener menos de :min caracteres',
                
            ]);
    
            if ($validator->fails()) {
                return view('registro_incidencia')
                    ->withErrors($validator)
                    ->with($request->all());             
            }
        }
        $nueva_incidencia = new Incidencia;

        $nueva_incidencia -> equipo = $request->input('equipo');
        $nueva_incidencia -> aula = $request->input('aula');
        $nueva_incidencia -> codigo = $request->input('codigo');
        $nueva_incidencia -> profesor_ID = $request->input('profesor_ID');
        $nueva_incidencia -> fecha = $request->input('fecha');
        $nueva_incidencia -> codigo_otros = $request->input('otros');
        if($request->input('obs_usu')!=Null){
            $nueva_incidencia -> obs_usu = $request->input('obs_usu');
        }
        $nueva_incidencia->save();
        return redirect('/home');
    }

/*
FUNCION PARA MODIFICAR UNA INCIDENCIA
*/
    public function modificar_incidencia(Request $request) {
        if(!Auth::user()){
            return view("error",['error'=>"NO PUEDES ACCEDER A ESTA RUTA SIN ESTAR LOGUEADO"]);
        }
        $request->flash();
        if(Auth::user()->admin){

            $incidencia= Incidencia::select('id','estado','obs_adm','admin')->where('id', $request->id)->orderBy('fecha', 'desc')->get();

            $validator = Validator::make($request->all(), [
                'estado' =>[
                    'in:pendiente,finalizado,cancelado',
                ],
                'obs_adm' =>[
                    'max:255',
                ]
            ],
            [
                //Mensajes de error en un campo en especifico
                'estado.in' =>'No has seleccionado un estado valido',
                'obs_adm.max' => 'El campo observaciones del administrador no puede tener mas de :max caracteres',
                //Mensajes de error generales
                'required' => 'El campo :attribute es obligatorio',
                'between' => 'El :attribute valor :input no esta entre :min - :max.',
                'max' => 'El :attribute no puede superar los :max caracteres',
                'min' => 'El :attribute no puede tener menos de :min caracteres',
                
            ]);

            if ($validator->fails()) {
                return view('mod_incid_admin',['todas_las_incidencias'=>$incidencia])
                    ->withErrors($validator)
                    ->with($request->all());
                            
            }else{
                //Aqui usamos el first() por que necesitamos coger solo una y con una lista no nos vale
                $incidencia= Incidencia::select('id','estado','obs_adm','admin')->where('id', $request->id)->orderBy('fecha', 'desc')->first();
                $estado="igual";
                if($request->estado!=Null){
                    $incidencia->estado = $request->estado;
                    $estado=$incidencia["estado"];
                }
                $obs="no_igual";
                
                if($incidencia->obs_adm == $request->obs_adm){
                    $obs="igual";
                }else{
                    $incidencia->obs_adm = $request->obs_adm;
                }
                if($request->obs_adm==null){
                    $incidencia->obs_adm="a esperas del admin";
                }
                $incidencia->admin = Auth::user()->id;
                //una vez cambiado todos los parametros que queremos lo guardamos
                $incidencia->save();

                $datos_incidencia_para_correo=Incidencia::select('id','profesor_ID','equipo')->where('id', $request->id)->orderBy('fecha', 'desc')->first();
                $profesor=profesores::select('id','email')->where('id', $datos_incidencia_para_correo->profesor_ID)->first();
                $email=$profesor["email"];
                $equipo=$datos_incidencia_para_correo["equipo"];
                //entramos en el controlador de EmailController con las variables guardadas en la sesion
                return redirect('laravel-send-email')->with('email', $email)->with('estado',$estado)->with('equipo',$equipo)->with('obs',$obs);
            }

        }else{

            $incidencia= Incidencia::select('id','profesor_ID','estado')->where('id', $request->id)->orderBy('fecha', 'desc')->first();
            if(Auth::user()->can('actuar_sobre_la_incidencia',$incidencia)){
                return view("error",['error'=>"ERROR LA INCIDENCIA NO ES SUYA"]);
            }
            if($incidencia->estado!="pendiente"){
                return view("error",['error'=>"ERROR NO SE PUEDE MODIFICAR UNA INCIDENCIA QUE ESTA CERRADA"]);
            }
            
            //Cogemos la incidencia teniendo en cuenta el id de la incidencia del formulario
            //Aqui usamos el get() por que necesitamos cogerlo en una lista de incidencias
            $incidencia= Incidencia::select('id','equipo','profesor_ID','aula','codigo','codigo_otros','fecha','estado','obs_usu')->where('id', $request->id)->orderBy('fecha', 'desc')->get();
            $incidencia=$incidencia[0];
            $codigo="";
            if($request->codigo==Null){
               $codigo=$incidencia->codigo;
            }else{
                $codigo=$request->codigo;
            }
            $incidencia= Incidencia::select('id','equipo','profesor_ID','aula','codigo','codigo_otros','fecha','estado','obs_usu')->where('id', $request->id)->orderBy('fecha', 'desc')->get();
            if($codigo=="10.Otros/ Beste batzu"){
                $validator = Validator::make($request->all(), [
                    'equipo' => [
                        'required','min:3','max:10','regex:/^hz[0-9]*$/',
                    ],
                    'aula' =>[
                        'required','min:3','max:5','regex:/^[0-9]*$/',
                    ],
                    'codigo' =>[
                        'in:10.Otros/ Beste batzu',
                    ],
                    'otros' =>[
                        'required','max:255',
                    ],
                    'obs_usu' =>[
                        'max:255',
                    ]
                ],
                [
                    //Mensajes de error en un campo en especifico
                    'equipo.regex' => 'El campo equipo no se ha escrito correctamente es necesario un hz seguido de uno a ocho numeros',
                    'aula.regex' => 'En el campo aula solo se admiten numeros',
                    'codigo.in' =>'No has seleccionado un error valido',
                    'obs_usu.max' => 'El campo observaciones del usuario no puede tener mas de :max caracteres',
                    'otros.max' => 'El campo otro error no puede tener mas de :max caracteres',
                    //Mensajes de error generales
                    'required' => 'El campo :attribute es obligatorio',
                    'between' => 'El :attribute valor :input no esta entre :min - :max.',
                    'max' => 'El :attribute no puede superar los :max caracteres',
                    'min' => 'El :attribute no puede tener menos de :min caracteres',
                    
                ]);
                if ($validator->fails()) {
                    return view('mod_incidencias',['todas_las_incidencias'=>$incidencia])
                        ->withErrors($validator)
                        ->with($request->all()); 
                                
                }else{
                    //Aqui usamos el first() por que necesitamos coger solo una y con una lista no nos vale
                    $incidencia= Incidencia::select('id','equipo','profesor_ID','aula','codigo','codigo_otros','fecha','estado','obs_usu')->where('id', $request->id)->orderBy('fecha', 'desc')->first();
                    $incidencia->equipo = $request->equipo;
                    $incidencia->aula = $request->aula;
                    if($request->codigo!=Null){
                        $incidencia->codigo = $request->codigo;
                    }
                    $incidencia->codigo_otros = $request->otros;
                    if($request->obs_usu!=Null){
                        $incidencia->obs_usu = $request->obs_usu;
                    }else{
                        $incidencia->obs_usu ="sin observaciones";
                    }

                    //una vez cambiado todos los parametros que queremos lo guardamos
                    $incidencia->save();
                    return redirect('/home');
                }
            }else{

                $validator = Validator::make($request->all(), [
                    'equipo' => [
                        'required','min:3','max:10','regex:/^hz[0-9]*$/',
                    ],
                    'aula' =>[
                        'required','min:3','max:5','regex:/^[0-9]*$/',
                    ],
                    'codigo' =>[
                        'in:1.No se enciende la CPU/ CPU ez da pizten,2.No se enciende la pantalla/Pantaila ez da pizten,3.No entra en mi sesión/ ezin sartu nere erabiltzailearekin,4.No navega en Internet/ Internet ez dabil,5.No se oye el sonido/ Ez da aditzen,6.No lee el DVD/CD,7.Teclado roto/ Tekladu hondatuta,8.No funciona el ratón/Xagua ez dabil,9.Muy lento para entrar en la sesión/oso motel dijoa',
                    ],
                    'obs_usu' =>[
                        'max:255',
                    ]
                ],
                [
                    //Mensajes de error en un campo en especifico
                    'equipo.regex' => 'El campo equipo no se ha escrito correctamente es necesario un hz seguido de uno a ocho numeros',
                    'aula.regex' => 'En el campo aula solo se admiten numeros',
                    'codigo.in' =>'No has seleccionado un error valido',
                    'obs_usu.max' => 'El campo observaciones del usuario no puede tener mas de :max caracteres',
                    //Mensajes de error generales
                    'required' => 'El campo :attribute es obligatorio',
                    'between' => 'El :attribute valor :input no esta entre :min - :max.',
                    'max' => 'El :attribute no puede superar los :max caracteres',
                    'min' => 'El :attribute no puede tener menos de :min caracteres',
                    
                ]);
                if ($validator->fails()) {
                    return view('mod_incidencias',['todas_las_incidencias'=>$incidencia])
                        ->withErrors($validator)
                        ->with($request->all());
                                
                }else{
                    //Aqui usamos el first() por que necesitamos coger solo una y con una lista no nos vale
                    $incidencia= Incidencia::select('id','equipo','profesor_ID','aula','codigo','codigo_otros','fecha','estado','obs_usu')->where('id', $request->id)->orderBy('fecha', 'desc')->first();
                    $incidencia->equipo = $request->equipo;
                    $incidencia->aula = $request->aula;
                    if($request->codigo!=Null){
                        $incidencia->codigo = $request->codigo;
                    }
                    $incidencia->codigo_otros = Null;
                    if($request->obs_usu!=Null){
                        $incidencia->obs_usu = $request->obs_usu;
                    }else{
                        $incidencia->obs_usu ="sin observaciones";
                    }
                
                    //una vez cambiado todos los parametros que queremos lo guardamos
                    $incidencia->save();
                    return redirect('/home');
                }
            }
        }    
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    //Funcion para loguearse con google
    public function handleGoogleCallback()
    {
        try {
            $user = Socialite::driver('google')->user();
            $finduser = profesores::where('google_id', $user->id)->first();

            if($finduser){

                Auth::login($finduser);

                return redirect('/home');

            }else{
                if((explode('@',$user->email)[1] === 'plaiaundi.net')||(explode('@',$user->email)[1] === 'plaiaundi.com')||(explode('@',$user->email)[0] === 'julper23')){
                    $newUser = profesores::create([

                        'name' => $user->name,
                        'email' => $user->email,
                        'google_id'=> $user->id,
                        'avatar'=> $user->avatar,
                        'avatar_original'=> $user->avatar_original,
                    ]);

                    Auth::login($newUser);

                    return redirect('/home');
                   
                }else{
                    return redirect('https://accounts.google.com/logout')->to('/');
                }
            }

        } catch (Exception $e) {

            //dd($e->getMessage()); esta funcion recoge el error y lo muestra en pantalla.
            return redirect('/home');
        }

    }
}
