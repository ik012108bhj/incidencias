<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incidencia extends Model
{
    protected $table = 'incidencias';
    protected $fillable = [
        'id','equipo','aula','codigo','codigo_otros',"profesor_ID","fecha","obs_usu","obs_adm","admin"
    ];
}
