<?php

namespace App\Policies;

use App\profesores;
use App\Incidencia;
use Illuminate\Auth\Access\HandlesAuthorization;

class PoliticaProfesor
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function actuar_sobre_la_incidencia(profesores $profesor, Incidencia $incidencia)
    {
        return $profesor->id == $incidencia->profesor_ID;

    }
    public function es_admin(profesores $profesor , Incidencia $incidencia)
    {
        
        return $profesor->admin == 1;

    }

}
