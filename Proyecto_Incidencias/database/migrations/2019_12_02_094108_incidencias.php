<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Incidencias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidencias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('aula');
            $table->string('codigo');
            $table->string('codigo_otros')->nullable();
            $table->string('equipo');
            $table->unsignedBigInteger('profesor_ID');
            $table->foreign('profesor_ID')
                    ->references('id')-> on('profesores')
                    ->onUpdate('cascade')
                    ->onDelete('cascade')
            ;
            $table->string('estado')->default("pendiente");
            $table->string('fecha');
            $table->string('obs_usu')->default("sin observaciones");
            $table->string('obs_adm')->default("a esperas del admin");
            $table->string('admin')->default("a esperas del admin");
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('incidencias');
    }
}
