<?php

use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Http\Request;


//Pagina principal de la aplicacion
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//home normal
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home', 'HomeController@index')->name('home');
Route::get('/ver', 'HomeController@index')->name('home');
Route::post('/ver', 'HomeController@index')->name('home');

//home pendientes
Route::get('/incidencias_pendientes', 'HomeController@pendientes')->name('home');;
Route::post('/incidencias_pendientes', 'HomeController@pendientes')->name('home');

//home finalizados
Route::get('/incidencias_finalizadas', 'HomeController@finalizadas')->name('home');
Route::post('/incidencias_finalizadas', 'HomeController@finalizadas')->name('home');

//home cancelados
Route::get('/incidencias_canceladas', 'HomeController@canceladas')->name('home');
Route::post('/incidencias_canceladas', 'HomeController@canceladas')->name('home');

//Conectarse con google
Route::get('auth/google', 'GoogleController@redirectToGoogle');
Route::get('auth/google/callback', 'GoogleController@handleGoogleCallback');

//Desconectarse con google
Route::get('auth/logout', 'Auth\AuthController@logout');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

//Crear incidencia
Route::get('/crear_incidencia', 'GoogleController@vista_crear_incidencia');
Route::post('/crea_incid', 'GoogleController@crear_incidencia');

//Modificar incidencia
Route::post('modificar_incidencia','HomeController@modificar_incidencia');
Route::post('/mod_incid', 'GoogleController@modificar_incidencia');

//Borrar incidencia
Route::post('borrar_incidencia','HomeController@borrar_incidencia');

//Ver incidencia
Route::post('/ver_incidencia', 'HomeController@ver_incidencia');

//Mandar correo electronico
Route::get('laravel-send-email', 'EmailController@sendEMail');





