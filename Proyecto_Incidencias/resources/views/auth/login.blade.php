@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row justify-content-center">

        <div class="col-md-8">

            <div class="card">

                <div class="card-header">Se ha producido un error prueba a conectarte con una cuenta de plaiaundi.net, si no contacta con el administrador</div>

                <a href="{{ url('auth/google') }}" style="margin-top: 20px;" class="btn btn-lg btn-success btn-block">

                    <strong>Desloguearse</strong>

                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                    <iframe id="logoutframe" src="https://accounts.google.com/logout" style="display: none"></iframe>
                </form>
                                
            </div>

        </div>

    </div>

</div>

@endsection
