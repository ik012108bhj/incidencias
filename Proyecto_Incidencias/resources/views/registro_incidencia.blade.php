<style>

  .form td{
    font-size: 20px;
    border: 1px solid black;
    padding-left:20px;
    height: 150px;
  }
  #volver{
    padding:auto;
    margin:auto;
  }
  td{
    vertical-align: center;
  }

  .pequeño{
    height:140px;
  }
  .form{

    width: 90%;
  }
  table{
    vertical-align: middle;
    margin:auto;
  }
 .form tr:nth-child(odd){background-color: #C9F9D9;}
</style>
<script>
  function otras_opciones(){
    var error=document.getElementById("codigo").value;
    var e_otros="10.Otros/ Beste batzu";
    if(error==e_otros){
      document.getElementById("otros").removeAttribute("disabled");
      document.getElementById("otros").setAttribute("required","enabled");
    }else{
      document.getElementById("otros").setAttribute("disabled","disabled");
      document.getElementById("otros").removeAttribute("required");
    }
  }

</script>

@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">

          <div class="card-header" align="left" >
            <center><h1> Crear Incidencia</h1></center>
          </div>

          <div class="card-body">
            @if (session('status'))
              <div class="alert alert-success" role="alert">
                {{ session('status') }}
              </div>
            @endif

            <form action="crea_incid" method="post" id="form">
              @csrf
              <table class="form">
                <tr>
                  <td class="pequeño">
                    <label for="equipo">Equipo <span><em>*</em></span></label><br>
                    <center><input type="text" name="equipo" class="form-input" id="equipo" form="form" value="{{ old('equipo') }}" required/><center>
                  </td>
                  <td class="pequeño">
                    <label for="aula">Aula <span><em>*</em></span></label><br>
                    <center><input type="text" name="aula" class="form-input" form="form" value="{{ old('aula') }}" required/><center>
                  </td>
                </tr>
                <tr>
                  <td colspan="2" >
                    <center><select id="codigo" name="codigo" onchange="otras_opciones()" required>
                      <option disabled selected value="">Codigo De La Averia</option>    
                      <option value="1.No se enciende la CPU/ CPU ez da pizten">1. No se enciende la CPU/ CPU ez da pizten</option>
                      <option value="2.No se enciende la pantalla/Pantaila ez da pizten">2. No se enciende la pantalla/Pantaila ez da pizten</option>
                      <option value="3.No entra en mi sesión/ ezin sartu nere erabiltzailearekin">3. No entra en mi sesión/ ezin sartu nere erabiltzailearekin</option>
                      <option value="4.No navega en Internet/ Internet ez dabil">4. No navega en Internet/ Internet ez dabil</option>
                      <option value="5.No se oye el sonido/ Ez da aditzen">5. No se oye el sonido/ Ez da aditzen</option>
                      <option value="6.No lee el DVD/CD">6. No lee el DVD/CD</option>
                      <option value="7.Teclado roto/ Tekladu hondatuta">7. Teclado roto/ Tekladu hondatuta</option>
                      <option value="8.No funciona el ratón/Xagua ez dabil">8. No funciona el ratón/Xagua ez dabil</option>
                      <option value="9.Muy lento para entrar en la sesión/oso motel dijoa">9. Muy lento para entrar en la sesión/oso motel dijoa</option>
                      <option value="10.Otros/ Beste batzu">10. Otros/ Beste batzu</option>
                    </select><br>
                    <textarea rows="2" cols="30" name="otros" form="form" id="otros" style="resize: none;" disabled>{{ old('otros') }}</textarea><center>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <center><label for="obs_usu">Observaciones</label><br>
                    <textarea rows="2" cols="30" name="obs_usu" form="form" style="resize: none;" >{{ old('obs_usu') }}</textarea><center>
                  </td>
                </tr>
              </table>

              <input type="hidden" name="profesor_ID" class="form-input" value="{{ Auth::user()->id }}"required/>
              <input type="hidden" name="fecha" class="form-input" value="<?php echo date('d-m-Y'); ?>"required/>

              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif 

              <br>
              <table>
                <tr>
                  <td>   
                    <center> <input class="form-btn" name="submit" type="submit" value=" Aceptar" /></center>
                    </form>
                  </td>
                  <td>
                    <center><form action="home" method="post" id="volver">@csrf
                      <input class="form-btn" name="submit" type="submit" value="Cancelar" />
                    </form></center>
                  </td>
                </tr>
              </table>
          </div>

        </div>
      </div>
    </div>
  </div>
@endsection