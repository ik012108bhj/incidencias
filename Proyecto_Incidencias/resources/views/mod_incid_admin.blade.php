<style>

  .form td{
    font-size: 20px;
    border: 1px solid black;
    padding-left:20px;
    height: 180px;
  }

  #volver{
    padding:auto;
    margin:auto;
  }

  td{
    
  }

  table{
    vertical-align: middle;
    margin:auto;
  }

  .form tr:nth-child(odd){background-color: #C9F9D9;}

</style>

<script>
  //Funcion para activar o desactivar el textarea otros
  function otras_opciones(){
    var error=document.getElementById("codigo").value;
    var e_otros="10.Otros/ Beste batzu";

    if(error==e_otros){
      document.getElementById("otros").removeAttribute("disabled");
      document.getElementById("otros").setAttribute("required","enabled");
    }else{
      document.getElementById("otros").setAttribute("disabled","disabled");
      document.getElementById("otros").removeAttribute("required");
    }
  }

</script>

@extends('layouts.app')

@section('content')

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">

          <div class="card-header" align="left" >
            <center><h1> Modificar Incidencia</h1></center>
          </div>

          <div class="card-body">
            @if (session('status'))
              <div class="alert alert-success" role="alert">
                {{ session('status') }}
              </div>
            @endif

            <form action="mod_incid" method="post" id="form">
              @csrf
              @foreach ($todas_las_incidencias as $incidencia)
                <table style="width:80%;" class=form>

                  <tr>
                    <td colspan="2" align="center">
                      <label for="equipo">Estado</label><br><select name="estado" style="width:40%;">
                        <option disabled selected value="{{$incidencia->estado}}">{{$incidencia->estado}}</option>    
                        <option value="pendiente">pendiente</option>
                        <option value="finalizado">finalizado</option>
                        <option value="cancelado">cancelado</option>
                      </select>
                    </td>
                  </tr>

                  <tr>
                    <td colspan="2">
                      <center><label for="equipo">Observaciones</label>
                      <br>
                      <textarea rows="4" cols="30" name="obs_adm" style="resize: none;" form="form">{{ old('obs_adm') ?? $incidencia->obs_adm }}</textarea>   <center>
                    </td>
                  </tr>

                </table>

                <input type="hidden" name="profesor_ID" class="form-input" value="{{ Auth::user()->id }}"required/>
                <input type="hidden" name="fecha" class="form-input" value="<?php echo date('d-m-Y'); ?>"required/>
                <input type="hidden" name="id" value="{{$incidencia->id}}"></input>

                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif  

                <br>

                <table>
                  <tr>
                    <td>   
                    <center> <input class="form-btn" name="submit" type="submit" value=" Aceptar" /></center>
                    </form></td>
                    <td>
                    <center> <form action="home" method="post" id="volver">@csrf
                    <input class="form-btn" name="submit" type="submit" value="Cancelar" />
                    </form> </center></td>
                  </tr>
                </table>
              @endforeach
          </div>

        </div>
      </div>
    </div>
  </div>

@endsection