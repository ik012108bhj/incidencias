<style>

    table{
        width: 100%;
    }

    td{
      width: 100px;
      font-size: 15px;
      text-align: center;
    }

    table tr:nth-child(even){background-color: #C9F9D9;}
    
</style>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" align="left" >
                        <table >
                            <tr>
                                <td style="width: 79%;">
                                    <a href="{{ url("home") }}" style="margin-top: 10px;" class="btn btn-lg btn-success btn-block">
                                        <strong>INCIDENCIAS</strong>
                                    </a>
                                </td>

                                <td style="width: 2%;"></td>

                                <td style="width: 19%;">
                                    <!--Menu para filtrar las incidencias por el estado-->
                                    <div class="dropright " align="right" style="margin-top:10px;">
                                        <button class="btn btn-lg btn-success btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:100%;">
                                            Estado
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a href="{{ url("home") }}" class="dropdown-item"><strong>Todos</strong></a>
                                            <a href="{{ url("incidencias_pendientes") }}"  class="dropdown-item"><strong>Pendiente</strong></a>
                                            <a href="{{ url("incidencias_finalizadas") }}"  class="dropdown-item"><strong>Finalizado</strong></a>
                                            <a href="{{ url("incidencias_canceladas") }}" class="dropdown-item"><strong>Cancelado</strong></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <table width="100%">
                            <tr>
                                <th style="border:1px solid black; text-align: center; vertical-align: middle;">equipo</th> 
                                <th style="border:1px solid black; text-align: center; vertical-align: middle;">aula</th>                                
                                <th style="border:1px solid black; text-align: center; vertical-align: middle;">error</th>
                                <th style="border:1px solid black; text-align: center; vertical-align: middle;">profesor</th>
                                <th style="border:1px solid black; text-align: center; vertical-align: middle;">fecha</th> 
                                <th style="border:1px solid black; text-align: center; vertical-align: middle;">estado</th>
                                <th style="border:1px solid black; text-align: center; vertical-align: middle;">VER</th> 
                                <th style="border:1px solid black; text-align: center; vertical-align: middle;">MODIFICAR</th>
                            </tr>
                            @foreach ($todas_las_incidencias as $incidencia)
                                <tr>
                                    <td style="border:1px solid black; padding: 10px; vertical-align: middle;">{{$incidencia->equipo}}</td>
                                    <td style="border:1px solid black; padding: 10px; vertical-align: middle;">{{$incidencia->aula}}</td>
                                    <td style="border:1px solid black; padding: 10px; vertical-align: middle;">{{$incidencia->codigo}}</td>
                                    
                                    @foreach($listado_profesores as $profesor)
                                        @if($incidencia->profesor_ID==$profesor->id)
                                            <td style="border:1px solid black; padding: 10px; vertical-align: middle;">{{$profesor->name}}</td>
                                        @endif
                                    @endforeach
                                    <td style="border:1px solid black; padding: 10px; vertical-align: middle;">{{$incidencia->fecha}}</td>
                                    <td style="border:1px solid black; padding: 10px; vertical-align: middle;">{{$incidencia->estado}}</td>

                                    <!--Boton ver incidencia-->
                                    <td style="border:1px solid black; text-align: center; vertical-align: middle;">
                                        <form action="ver_incidencia" method="post" id="form_mod">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$incidencia->id}}"></input>
                                        <button type="submit" style="margin-top: 20px;" class="btn btn-lg btn-success"><strong><img src="/images/ver.png" height="42" width="42" alt="Ver"></strong></button>
                                        </form>
                                    </td>

                                    <!--Boton modificar incidencia -->
                                    <td style="border:1px solid black; text-align: center; vertical-align: middle;">
                                        <form action="modificar_incidencia" method="post" id="form_mod">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$incidencia->id}}"></input>
                                            
                                        <button type="submit" style="margin-top: 20px;" class="btn btn-lg btn-success"><strong><img src="/images/mod.png" height="42" width="42" alt="Modificar"></strong></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <!--PAGINACION-->
                        {{ $todas_las_incidencias->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection