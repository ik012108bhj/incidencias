<style>

  td{
    width: 550px;
    font-size: 20px;
    border: 1px solid black;
    padding-left:20px;
    height: 50px;
  }

  table{
    vertical-align: middle;
    margin:auto;
    width: 80%;
  }

  table tr:nth-child(odd){background-color: #C9F9D9;}
</style>

@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        @foreach ($todas_las_incidencias as $incidencia)
          <div class="card-header" align="left" >
            <center><h1>Incidencia nº {{$incidencia->id}}</h1></center>
          </div>

          <div class="card-body">
            @if (session('status'))
              <div class="alert alert-success" role="alert">
                {{ session('status') }}
              </div>
            @endif
            <table>
              
              <tr>
                <td>Aula</td>
                <td>{{$incidencia->aula}}</td>
              </tr>

              <tr>
                <td>Error</td>
                <td>{{$incidencia->codigo}}</td>
              </tr>

              @if($incidencia->codigo_otros!=null)
                <tr>
                  <td>Explicacion Del Error</td>
                  <td>{{$incidencia->codigo_otros}}</td>
                </tr>
              @endif

              <tr>
                <td>Equipo</td>
                <td>{{$incidencia->equipo}}</td>
              </tr>

              <tr>
                <td>Profesor</td>
                <td>{{$profesor->name}}</td>
              </tr>

              <tr>
                <td>Estado</td>
                <td>{{$incidencia->estado}}</td>
              </tr>

              <tr>
                <td>Fecha</td>
                <td>{{$incidencia->fecha}}</td>
              </tr>

              <tr>
                <td>Observacion Del Usuario</td>
                <td>{{$incidencia->obs_usu}}</td>
              </tr>

              <tr>
                <td>Observacion Del Administrador</td>
                <td>{{$incidencia->obs_adm}}</td>
              </tr>

              <tr>

                <td>Admin</td>

                @if($incidencia->admin=="a esperas del admin")
                  <td>{{$incidencia->admin}}</td>
                @else
                  <td>{{$admin->name}}</td>
                @endif

              </tr>
            </table>
            <br>
            <form action="home" method="get" id="form">
              <center><input class="form-btn" name="submit" type="submit" value="Volver" /></center>
            </form>
          </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
@endsection